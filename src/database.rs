use once_cell::sync::Lazy;
use std::collections::HashMap;
use std::sync::Mutex;
use std::sync::atomic::{Ordering, AtomicI64};
use chrono::{DateTime, Local};

const MEETING_TTL: i64 = 5;

static SEQUENCE: AtomicI64 = AtomicI64::new(0);
static MEETINGS: Lazy<Mutex<HashMap<i64, Vec<Meeting>>>> = Lazy::new(|| Mutex::new(HashMap::new()));

#[derive(Debug, Clone)]
pub struct Meeting {
    pub id: i64,
    pub chat_id: i64,
    pub created_when: DateTime<Local>,
    pub message_id: i32,
    pub url: String,
    pub day: Option<i32>,
    pub hour: Option<i32>,
    pub minutes: Option<i32>,
    pub expand: bool
}

impl  Meeting {
    pub fn new(chat_id: i64, message_id: i32, url: &str) -> Self {
        let _ = &SEQUENCE.fetch_add(1, Ordering::SeqCst);

        Meeting {
            id: SEQUENCE.load(Ordering::SeqCst),
            chat_id: chat_id,
            created_when: Local::now(),
            message_id: message_id,
            url: String::from(url),
            day: None,
            hour: None,
            minutes: None,
            expand: false
        }
    }
}

pub struct Database {

}

impl Database {
    pub fn new() -> Self {
        Database { }
    }

    pub fn cleanup(&self) -> i32 {
        let now = Local::now();

        let mut meetings =  MEETINGS.lock().unwrap();
        let mut count: i32 = 0;

        meetings.retain(|_key, value| {
            value.retain(|meeting| {
                let diff = now.signed_duration_since(meeting.created_when);
                if diff.num_minutes() > MEETING_TTL {
                    return false;
                }
                count += 1;
                return true;
            });
            return !value.is_empty();
        });

        return count;
    }

    pub fn get_meeting(&self, chat_id: &i64, meeting_id: &i64) -> Option<Meeting> {
        let meetings =  MEETINGS.lock().unwrap();
        let vector = meetings.get(chat_id);
        if vector.is_none() {
            return None;
        }

        let vector = vector.unwrap();
        for meeting in vector {
            if meeting.id == *meeting_id {
                return Some(meeting.clone());
            }
        }
        return None;
    }

    pub fn push_meeting(&mut self, meeting: Meeting) {
        let mut meetings =  MEETINGS.lock().unwrap();
        let vector = meetings.get_mut(&meeting.chat_id);
        if vector.is_none() {
            meetings.insert(meeting.chat_id, vec![meeting]);
        } else {
            let vector = vector.unwrap();
            vector.retain(|mtg| { meeting.id != mtg.id });
            vector.push(meeting);
        }
    }

}
