mod bot;
mod database;

use once_cell::sync::Lazy;
use std::sync::Mutex;
use std::time::{Duration};
use teloxide::prelude::*;

use crate::database::Database;

static BOT: Lazy<AutoSend<Bot>> = Lazy::new(|| Bot::from_env().auto_send());
static DB: Lazy<Mutex<Database>> = Lazy::new(|| Mutex::new(Database::new()));

#[tokio::main]
async fn main() {
    run().await;
}

async fn run() {
    teloxide::enable_logging!();
    log::info!("Starting ZoomNotifierBot...");

    tokio::spawn(async { crate::bot::start().await });

    loop {
        let total = DB.lock().unwrap().cleanup();
        log::debug!("Meetings in memory. Total count: {}", total);
        tokio::time::sleep(Duration::from_secs(60 * 1)).await;
    }
}
