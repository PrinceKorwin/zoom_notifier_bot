type Update<T> = UpdateWithCx<AutoSend<Bot>, T>;

use chrono::{Duration, Local};
use teloxide::types::*;
use teloxide::prelude::*;

use crate::BOT;
use crate::DB;
use crate::database::Meeting;

pub async fn callback_handler(callback: Update<CallbackQuery>) {
    let message = &callback.update.message.unwrap();
    let chat_id = message.chat_id();
    let message_id = message.id;
    let data = &callback.update.data.unwrap();

    log::info!("Callback chat_id:{}, message_id:{}, data:{}", &chat_id, &message_id, data);

    callback.requester.inner()
        .answer_callback_query(&callback.update.id)
        .send()
        .await.log_on_error().await;

    let a: Vec<&str> = data.split(':').collect();
    let meeting_id = a.get(0).unwrap().parse::<i64>().unwrap();
    let meeting_field = a.get(1).unwrap();
    let meeting_value = a.get(2).unwrap().parse::<i32>().unwrap();

    let meeting = DB.lock().unwrap().get_meeting(&chat_id, &meeting_id);
    if meeting.is_none() {
        BOT.clone()
            .edit_message_text(
                chat_id,
                message_id,
                "Something goes wrong: can't found context with your action")
            .send().await.log_on_error().await;
        return;
    }
    let mut meeting = meeting.unwrap();

    if meeting_field.eq(&"day") {
        meeting.day = Some(meeting_value);
    } else if meeting_field.eq(&"hour") {
        meeting.hour = Some(meeting_value);
    } else if meeting_field.eq(&"minutes") {
        meeting.minutes = Some(meeting_value);
    }
    DB.lock().unwrap().push_meeting(meeting.clone());

    if  meeting.day.is_none() || meeting.hour.is_none() || meeting.minutes.is_none() {
        BOT.clone().edit_message_reply_markup(chat_id, message_id)
            .reply_markup(draw_keyboard(&meeting))
            .send()
            .await.log_on_error().await;

    } else {
        let date_time = Local::today().and_hms(
            meeting.hour.unwrap() as u32, meeting.minutes.unwrap() as u32, 0)
            .checked_add_signed(Duration::days(meeting.day.unwrap() as i64));
        let date_time = date_time.unwrap();

        let buttons = draw_link(
            &date_time.format("%d.%m.%Y at %H:%M").to_string(),
            &meeting.url
        );

        if let Ok(_ret) = BOT.clone().edit_message_text(chat_id, message_id, "Meeting registered:")
            .reply_markup(buttons).send().await {
            log::debug!("Zoom meeting registered");
        } else {
            log::error!("Can't register Zoom meeting!");
            BOT.clone().edit_message_text(chat_id, message_id, "Something goes wrong: Please check URL")
                .send().await.log_on_error().await;
        }

        BOT.clone().delete_message(chat_id, meeting.message_id).send().await.log_on_error().await;
    }
}

pub async fn message_handler(message: Update<Message>) {
    let message_id = message.update.id;
    let chat_id = message.chat_id();
    let text = message.update.text();

    log::info!("Message received from chat:{}, message_id:{}, text:{}", &chat_id, &message_id, &text.unwrap_or(""));

    if text.is_none() {
        return;
    }
    let text = text.unwrap();

    if text.starts_with("https://") {
        let meeting = Meeting::new(chat_id, message_id, text);
        DB.lock().unwrap().push_meeting(meeting.clone());

        message
            .answer("Meeting scheduled at:")
            // .parse_mode(ParseMode::MarkdownV2)
            .reply_markup(draw_keyboard(&meeting))
            .await
            .log_on_error()
            .await;

    } else {
        // Just do nothing
        // message
        //     .answer("Please enter Zoom Meeting URL")
        //     .await
        //     .log_on_error()
        //     .await;
    }
}

// Private functions ===============================================================================

fn draw_keyboard(meeting: &Meeting) -> InlineKeyboardMarkup {
    return if meeting.day.is_none() {
        InlineKeyboardMarkup::default().append_row(vec![
            { draw_button(&String::from("today"), format!("{}:day:0", meeting.id)) },
            { draw_button(&String::from("tomorrow"), format!("{}:day:1", meeting.id)) }
        ]).append_row(vec![
            { draw_button(&String::from("day after tomorrow"), format!("{}:day:2", meeting.id)) }
        ])

    } else if meeting.hour.is_none() {
        let mut buttons_1 = vec![];
        let mut buttons_2 = vec![];
        let mut buttons_3 = vec![];
        for hour in 9..=14 {
            buttons_1.push(draw_button(&format!("{}", hour), format!("{}:hour:{}", meeting.id, hour)));
        }
        for hour in 15..=20 {
            buttons_2.push(draw_button(&format!("{}", hour), format!("{}:hour:{}", meeting.id, hour)));
        }
        for hour in 21..=23 {
            buttons_3.push(draw_button(&format!("{}", hour), format!("{}:hour:{}", meeting.id, hour)));
        }
        InlineKeyboardMarkup::default()
            .append_row(buttons_1).append_row(buttons_2).append_row(buttons_3)

    } else {
        InlineKeyboardMarkup::default().append_row(vec![
            { draw_button(&String::from("00"), format!("{}:minutes:0", meeting.id)) },
            { draw_button(&String::from("10"), format!("{}:minutes:10", meeting.id)) },
            { draw_button(&String::from("15"), format!("{}:minutes:15", meeting.id)) },
            { draw_button(&String::from("20"), format!("{}:minutes:20", meeting.id)) }
        ]).append_row(vec![
            { draw_button(&String::from("30"), format!("{}:minutes:30", meeting.id)) },
            { draw_button(&String::from("40"), format!("{}:minutes:40", meeting.id)) },
            { draw_button(&String::from("45"), format!("{}:minutes:45", meeting.id)) },
            { draw_button(&String::from("50"), format!("{}:minutes:50", meeting.id)) },
        ])
    }
}

fn draw_button(text: &String, data: String) -> InlineKeyboardButton {
    InlineKeyboardButton::callback(text.clone(), data)
}

fn draw_link(text: &String, url: &String) -> InlineKeyboardMarkup {
    InlineKeyboardMarkup::default().append_row(vec![InlineKeyboardButton::url(text.clone(), url.clone())])
}
