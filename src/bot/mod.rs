mod handler;

use crate::BOT;
use handler::*;
type DispatcherHandler<T> = DispatcherHandlerRx<AutoSend<Bot>, T>;

use teloxide::types::*;
use teloxide::prelude::*;
use tokio_stream::wrappers::UnboundedReceiverStream;

pub async fn start() {
    Dispatcher::new(BOT.clone())
        .callback_queries_handler(|rx: DispatcherHandler<CallbackQuery>| {
            UnboundedReceiverStream::new(rx).for_each_concurrent(None, |callback| async move {
                callback_handler(callback).await;
            })
        })

        .messages_handler(|rx: DispatcherHandler<Message>| {
            UnboundedReceiverStream::new(rx).for_each_concurrent(None, |message| async move {
                message_handler(message).await;
            })
        })
        .dispatch()
        .await;
}
