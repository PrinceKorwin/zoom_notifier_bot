# Install & run on Linux Ubuntu

Install linkers and other dev-tool:
```
sudo apt install build-essential
```

Install pkg-config & libssl-dev:
```
sudo apt install pkg-config
sudo apt install libssl-dev
```

Check is Git allready installed:
```
git --version
```

Install Git if required:
```
sudo apt-add-repository ppa:git-core/ppa
sudo apt-get update
sudo apt-get install git
```

Install Rust:
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Configure Git (optional):
```
git config --global user.name "your_username"
git config --global user.email "your_email_address@example.com"
```

Clone repository:
```
git clone https://gitlab.com/PrinceKorwin/zoom_notifier_bot.git
```

Build Bot:
```
cd zoom_notifier_bot
cargo build --release
```

Run Bot:
```
export TELOXIDE_TOKEN=your_bot_token
./target/release/zoom_notifier_bot
```

Update Bot from Gitlab:
```
git pull origin master
```
